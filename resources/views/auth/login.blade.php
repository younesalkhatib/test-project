@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">{{ __('Sign In') }}</h5>
            <form class="form-signin" method="POST" action="{{ route('login') }}">
            @csrf
              <div class="form-label-group">
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
              @error('email')
                <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                </span>
              @enderror
              <label for="email">{{ __('E-Mail Address') }}</label> 
              </div>

              <div class="form-label-group">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label for="password">{{ __('Password') }}</label>
              </div>

              <div class="custom-control custom-checkbox mb-3">
                <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="custom-control-label" for="remember">
                        {{ __('Remember Me') }}
                </label>
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit"> {{ __('Login') }}</button>
              @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                   {{ __('Forgot Your Password?') }}
                </a>
                @endif
              <hr class="my-4">
              <button class="btn btn-danger btn-google btn-block text-uppercase" type="submit"><a class="fab fa-google mr-2"></a> Sign in with Google</button>
              <button class="btn btn-info btn-facebook btn-block text-uppercase" type="submit"><a href="{{ url('/redirect') }}" class="fab fa-facebook-f mr-2"> Sign in with Facebook</a></button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection