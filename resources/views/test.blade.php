
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

background:#fff;
	background: rgba(255,255,255, 0.5);
	-webkit-border-radius: 10px;
            border-radius: 10px;

<ul id="menu">
    <li data-menuanchor="firstPage" class="active"><a href="#firstPage">First slide</a></li>
    <li data-menuanchor="secondPage"><a href="#secondPage">Second slide</a></li>
    <li data-menuanchor="3rdPage"><a href="#3rdPage">Third slide</a></li>
    <li ><a href="{{ route('login') }}">Login</a></li>
    <li ><a href="{{ route('register') }}">Register</a></li>  
</ul>


<nav class="navbar navbar-expand-lg navbar-light bg-transparent">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="#firstPage">First slide</a>
      <a class="nav-item nav-link" href="#secondPage">Second slide</a>
      <a class="nav-item nav-link" href="#3rdPage">Third slide</a>
      <a class="nav-item nav-link" href="{{ route('login') }}">Login</a>
      <a class="nav-item nav-link" href="{{ route('register') }}">Register</a>
    </div>
  </div>
</nav>