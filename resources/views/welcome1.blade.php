<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Active Slide - fullPage.js</title>
    <meta name="author" content="Matthew Howell" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="fullPage continuous scrolling demo." />
    <meta name="keywords"  content="fullpage,jquery,demo,scroll,loop,continuous" />
    <meta name="Resource-type" content="Document" />


    <link rel="stylesheet" type="text/css" href="{{ url('css/fullpage.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('css/examples.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}" />

    <!--[if IE]>
        <script type="text/javascript">
             var console = { log: function() {} };
        </script>
    <![endif]-->
</head>
<body>


<ul id="menu">
@if (Route::has('login'))
    <li class="lang"><a>language</a></li>
    @auth
    <li ><a href="{{ route('home') }}">{{Auth::user()->name}}</a>
    @else
    <li ><a href="{{ route('register') }}">{{__('keys.reg')}}</a></li>
    <li ><a href="{{ route('login') }}">{{__('keys.login')}}</a></li>
    @endauth
    <li data-menuanchor="3rdPage"><a href="#3rdPage">{{__('keys.about')}}</a></li>
    <li data-menuanchor="secondPage"><a href="#secondPage">{{__('keys.news')}}</a></li>
    <li data-menuanchor="firstPage" class="active"><a href="#firstPage">{{__('keys.home')}}</a></li>
@endif
</ul>


<div id="fullpage">
    <div class="section" id="section0">
     <h1>{{__('keys.introduction')}}</h1>
        <div id="example">
        </div>
    </div>
    <div class="section active" id="section1">
        <div class="slide" id="slide1">
            <div class="intro">
                <h1>Slide 2.1</h1>
                <iframe data-src="auto-height.html"></iframe>
            </div>
        </div>
        <div class="slide active" id="slide2">
            <div class="intro">
                <h1>Slide 2.2</h1>
                <p>
                    We are using the class `active` on this section and in this particular horizontal slide. This way it will appear on the viewport on page load, instead of the 1st section 1st slide.
                </p>
                <p>You can apply the same logic to horizontal slides</p>
            </div>
        </div>
        <div class="slide" id="slide3">
            <h1>Slide 2.3</h1>
        </div>

    </div>
    <div class="section" id="section2">
        <div class="intro">
            <h1>Section 3</h1>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/examples.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/fullpage.js') }}"></script>
<script type="text/javascript">
    var myFullpage = new fullpage('#fullpage', {
        sectionsColor: ['#ff5f45', '#4BBFC3', '#7BAABE', 'whitesmoke', '#ccddff'],
        anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'],
        menu: '#menu',
        lazyLoad: true
    });
</script>

</body>
</html>