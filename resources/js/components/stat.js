import React from 'react';
import ReactDOM from 'react-dom';
import {Component} from 'react';
import axios from 'axios';
import AnimatedNumber from "animated-number-react";
//import FlipNumbers from 'react-flip-numbers';
//import { Link } from 'react-router-dom';
//import Pagination from "react-js-pagination";

export default class Stat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      stats: [],
      isLoaded: false,
      value: 150,
    };

  }


  componentDidMount() {

    fetch('http://corona-virus-stats.herokuapp.com/api/v1/cases/countries-search?search=lebanon&fbclid=IwAR0SRW7Hwp55yoOVZ2RGesXBFkaXxIByP_xfj8KhfTb59nTIA5mhtbLo7J0')    //,{mode: 'no-cors'} //,{ headers: {'Access-Control-Allow-Origin': '*', } }
    .then(response => response.json())
    .then(
      (response) => {
        console.log("younes",response)
        this.setState({
          isLoaded: true,
          stats: response.data.rows
        });
        console.log(this.state.stats)
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }
  // this.setState({stat:JSON.parse(response.data)});
  render() {
    const { error, isLoaded, stats } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div><h1>Loading...</h1></div>;
    } else {
      return (
        <ul>
          {stats.map(stat => (
            <li key={stat.country_abbreviation}>
              <h3>Corona News Lebanon</h3>
              <h3>total cases : <AnimatedNumber
                 value={stat.total_cases}
                 formatValue={this.formatValue}
                 /></h3>
              <h3>new cases : <AnimatedNumber
                 value={stat.new_cases}
                 formatValue={this.formatValue}
                 /></h3>
              <h3>total deaths : <AnimatedNumber
                 value={stat.total_deaths}
                 formatValue={this.formatValue}
                 /> </h3>
              <h3>total recovered : <AnimatedNumber
                 value={stat.total_recovered}
                 formatValue={this.formatValue}
                 /></h3>
           </li>
           
          ))}
        </ul>
    
      );
    }
  }
}                            
   
