import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Stat from './stat';
//import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
export default class Index extends Component {
    render() {
        return (
            <div className="container">
                <Stat />
            </div>
        );
    }
}

if (document.getElementById('example')) {
    ReactDOM.render(<Index />, document.getElementById('example'));
}
